import { Component } from '@angular/core';
import { MusicService } from './music-search/music.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = ' Angular 4';

  constructor(private service: MusicService) {
    // this.service.search('alice')
  }
  
  authorize() {
    let client_id = 'e266c4b8b48047878d5b5b7779e23128';
    let redirect_uri = window.location.origin; //'http://localhost:4200' 

    window.location.replace(`https://accounts.spotify.com/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}/&response_type=token`)
  }

}
