import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistsListItemComponent } from './playlists/playlists-list-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { JakkolwiekDirective } from './jakkolwiek.directive';

import { PlaylistsService } from './playlists/playlists.service'

import { MusicSearchModule } from './music-search/music-search.module'

import { routing } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { TestingComponent } from './testing.component'

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistsListItemComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    JakkolwiekDirective,
    PlaylistContainerComponent,
    TestingComponent,
  ],
  imports: [
    BrowserModule, // ngStyle, ngClass, etc..
    FormsModule,
    HttpModule,
    MusicSearchModule,
    routing,
  ],
  providers: [
    //{provide:PlaylistsService, useClass:PlaylistsService}
    PlaylistsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

