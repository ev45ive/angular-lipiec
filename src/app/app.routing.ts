import { RouterModule, Routes, Route } from '@angular/router'
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { PlaylistContainerComponent }  from './playlists/playlist-container.component'

const routes:Routes = [
  // Home / default path:
  { path:'', redirectTo:'playlists', pathMatch: 'full' },
  
  { path:'playlists', component: PlaylistsComponent, children:[
      { path: '', component: PlaylistContainerComponent},
      // /#/playlists/123  --> Playlist.id == 123
      { path: ':id', component: PlaylistContainerComponent}
  ] },
  
  // 404 - nie znaleziono strony
  { path: '**', redirectTo:'playlists', pathMatch:'full'}
]

export const routing = RouterModule.forRoot(routes,{
  //enableTracing: true,
  //useHash: true,
})