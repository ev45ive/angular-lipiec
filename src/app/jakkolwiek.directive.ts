import { Directive, Input, TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';

@Directive({
  selector: '[sgJakkolwiek]'
})
export class JakkolwiekDirective {

  // shJakkolwiek="hide" 
  @Input('sgJakkolwiek')
  set changehidden(hide){
    if(hide){
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl)
      }
    }
  }

  cache:ViewRef;
  
  constructor(private tpl:TemplateRef<any>, 
              private vcr:ViewContainerRef) { 
    
  }

}
