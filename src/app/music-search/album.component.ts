import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Album, AlbumImage } from './interfaces'

@Component({
  selector: 'div[sg-album]',
  template: `
      <img class="card-img-top" [src]="image">
      <div class="card-block">
        <h4 class="card-title">{{ album.name | shorten }}</h4>
      </div>
  `,
  styles: [`
    img{
      width:100%;
    }
  `]
})
export class AlbumComponent implements OnInit {

  @Input('sg-album')
  set setAlbum(album){
    this.album = album;
    this.image = album.images[0].url
  }
  
  album:Album

  image:AlbumImage

  @HostBinding('class.card')
  card = true;

  constructor() { }

  ngOnInit() {
  }

}
