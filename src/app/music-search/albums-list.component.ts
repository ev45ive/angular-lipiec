import { Component, OnInit, Input } from '@angular/core';
import { Album } from './interfaces'

@Component({
  selector: 'sg-albums-list',
  template: `
  <div class="card-group">
    <div *ngFor="let album of albums" [sg-album]="album"></div>
  </div>
  `,
  styles: [`
    .card{
      max-width:25%;
      min-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums:Album[] = []

  constructor() { }


  ngOnInit() {
  }

}
