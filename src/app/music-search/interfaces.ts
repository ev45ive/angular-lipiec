export interface Album{
    id: string;
    name: string;
    images: AlbumImage[]
    artists: AlbumArtist[]
}

export interface AlbumImage{
    height: number;
    width: number;
    url : string;
}

export interface AlbumArtist{
    id: string,
    name: string
}