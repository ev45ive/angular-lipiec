import { Component, OnInit } from '@angular/core';
import { Album } from './interfaces'
import { MusicService } from './music.service'
import { Subscription, Observable} from 'rxjs'

@Component({
  selector: 'sg-music-search',
  template: `
    <div class="row">
      <div class="col">
        <sg-search-form></sg-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <sg-albums-list [albums]="albums$ | async "></sg-albums-list>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  //albums: Album[] = []
  albums$:Observable<Album[]>;

  constructor(private service: MusicService) {
      //this.service.search('alice')
    this.albums$ = this.service.getAlbums$()   
  }



//  subscription:Subscription

  ngOnInit() {
     //this.subscription = this.service.getAlbums$().subscribe( albums => this.albums = albums)
  }
  ngOnDestroy(){
    //this.subscription.unsubscribe()
  }

}
