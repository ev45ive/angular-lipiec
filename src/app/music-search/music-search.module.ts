import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchFormComponent } from './search-form.component';
import { MusicSearchComponent } from './music-search.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumComponent } from './album.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'

import { MusicService } from './music.service';
import { ShortenPipe } from './shorten.pipe'

import { routing } from './music-search.routing'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent, 
    AlbumsListComponent, 
    AlbumComponent, 
    ShortenPipe
  ],
  exports:[
    MusicSearchComponent
  ],
  providers:[
    MusicService
  ]
})
export class MusicSearchModule { }
