import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service'
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, Validator, ValidatorFn, ValidationErrors, AbstractControl, AsyncValidatorFn } from '@angular/forms'

import { Observable } from 'rxjs'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/debounceTime'

@Component({
  selector: 'sg-search-form',
  template: `
    <form [formGroup]="queryForm">
      <div class="input-group">
        <input formControlName="query" class="form-control">
        <span class="input-group-btn">
          <button class="btn btn-default" [disabled]="queryForm.invalid || queryForm.pending">Search</button>
        </span>
      </div>
      <div *ngIf="queryForm.dirty || queryForm.touched">
        <p *ngIf="queryForm.controls.query.errors?.required"> Field is required </p>
        <p *ngIf="query.errors?.minlength"> Text it too short </p>
        <p *ngIf="query.errors?.censor"> Text is not allowed </p>
        <p *ngIf="queryForm.pending">Please wait, checking ...</p>
        <!-- <form-errors [field]="query"></form-errors> -->
      </div>
      <br />
    </form>
  `,
  styles: [`
    input.ng-touched.ng-invalid,
    input.ng-dirty.ng-invalid {
        border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup;
  query;

  constructor(private service: MusicService, private builder: FormBuilder) {

    const censor = (text): ValidatorFn => (control: AbstractControl): ValidationErrors => {

      return (control.value.search(text) > -1) ? {
        'censor': true
      } : null
    }

    const asyncCensor = (text): AsyncValidatorFn => (control: AbstractControl) => {

      // return http.get('...').map(response => response.ok? null : {'censor':true} )
      return Observable.create(observer => {
        setTimeout(() => {
          observer.next( (control.value.search(text) > -1) ? {
            'censor': true
          } : null )
          observer.complete()
        }, 2000)
      })
    }


    this.query = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ], [
      asyncCensor('batman')
    ]);

    this.queryForm = new FormGroup({
      'query': this.query
    })

    // lub:
    // this.queryForm = this.builder.group({
    //   query: ['',[Validators.required]]
    // })

    //http://reactivex.io/
    //http://reactivex.io/rxjs/manual/tutorial.html
    this.queryForm.get('query')
      .valueChanges
      .filter(query => query.length >= 3)
      .distinctUntilChanged()
      .debounceTime(400)
      .subscribe(query => this.search(query))

  }

  search(query) {
    this.service.search(query)
  }

  ngOnInit() {
  }

}
