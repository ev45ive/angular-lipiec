import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  //pure: false
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, length = 20): any {
    return value.length > length? (value.substr(0,length) + '...') : value
  }

}
