import { Directive, ElementRef, Input,
HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[sgHighlight]'
})
export class HighlightDirective {

  // pobieramy playlist.color
  @Input()
  sgHighlight
  
  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.active? this.sgHighlight : ''
  }

  active = false;

  @HostListener('mouseenter',['$event.target'])
  activate(target){
    this.active = true;
    // this.elem.nativeElement.style.borderLeftColor = this.sgHighlight
  }

  @HostListener('mouseleave',['$event.target'])
  deactivate(target){
    this.active = false;
    // this.elem.nativeElement.style.borderLeftColor = ''
  }

  constructor(private elem:ElementRef) {
  }

}
