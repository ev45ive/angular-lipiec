import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { PlaylistsService } from './playlists.service'

@Component({
  selector: 'sg-playlist-container',
  template: `
    <sg-playlist-details [playlist]="playlist$ | async " (save)="save($event)"></sg-playlist-details>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist$;

  constructor(private route:ActivatedRoute, private service: PlaylistsService, private router:Router) {
    // /#/playlist/123
    
    // Get id param from URL:
    // let id = route.snapshot.params['id']
    
    // Subscribe to new params
    // Get Playlist by ID:
    this.playlist$  = route.params
                           .pluck('id')
                           .switchMap( id => this.service.getPlaylist(id) )
  }

  save(playlist){
    // zapisz playliste
    this.service.savePlaylist(playlist); 
    // wybierz nowa / zmieniona playliste:
    this.router.navigate(['playlists', playlist.id])
  }

  ngOnInit() {
  }

}
