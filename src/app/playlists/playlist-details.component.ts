import { 
  Component, 
  OnInit, 
  Input, EventEmitter, Output
} from '@angular/core';
import { Playlist } from './interfaces'
import {PlaylistsService} from './playlists.service'

@Component({
  selector: 'sg-playlist-details',
  template: `
  <h4>Playlist Details</h4>

  <ng-template #noPlaylist>
    <p> Please select Playlist</p>
  </ng-template>

  <div [ngSwitch]="edit" *ngIf="playlist; else noPlaylist">

    <div *ngSwitchCase="false">
      <p> Name: {{playlist.name}}</p>
      <p> {{ playlist.favourite? 'Favourite' : 'Not Favourite'}} </p>
      <p [style.color]="playlist.color"> Color </p>
      <button class="btn btn-default" (click)=" edit = !edit ">Edit</button>
    </div>
    
    <div *ngSwitchCase="true">
     <form #formRef="ngForm" (ngSubmit)="save(formRef)">
        <div class="form-group" >
            <label>Name:</label>
            <input type="text" class="form-control" [ngModel]="playlist.name" name="name" required #nameRef="ngModel">
            <p *ngIf="nameRef.errors?.required">Field is required</p>
        </div>
        <div class="form-group">
            <label>Favourite:</label>
            <input type="checkbox" [ngModel]="playlist.favourite" name="favourite">
        </div>
        <div class="form-group">
            <label>Color:</label>
            <input type="color" [ngModel]="playlist.color" name="color">
        </div>
        <div class="float-right">
          <button class="btn btn-default" (click)=" edit = !edit ">Cancel</button>
          <input type="submit" class="btn btn-success" value="Save">
        </div>
      </form>
    </div>
  </div>
  `,
  styles: [],
  // inputs:[
  //   'playlist:playlist'
  // ]
})
export class PlaylistDetailsComponent implements OnInit {

  constructor(private service:PlaylistsService) { }

  save(formRef){
    if(formRef.valid){
      this.service.savePlaylist( Object.assign({}, this.playlist, formRef.value))
    }
  }
  
  edit = false;

  @Input('playlist')
  playlist:Playlist


  ngOnInit() {
  }

}
