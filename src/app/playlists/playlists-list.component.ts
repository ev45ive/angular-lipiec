import { 
  Component, 
  OnInit, 
  ViewEncapsulation,
  Input,
  Output, 
  EventEmitter 
} from '@angular/core';
import { Playlist } from './interfaces'

  // [style.borderLeftColor]="(active == playlist? playlist.color : 'black')"     
  // (mouseenter)="active = playlist"
  // (mouseleave)="active = false"

        // (click)="selectPlaylist(playlist)"
@Component({
  selector: 'sg-playlists-list',
  template: `
    <div class="list-group">
      <div *ngFor="let playlist of playlists" class="list-group-item color-border" 
        [class.active]="selected?.id == playlist?.id"
        
        [sgHighlight]="playlist.color"

        [routerLink]="[playlist.id]"

        >
        {{playlist.name}}
      </div>
    </div>
  `,
  styles: [`
    .color-border{
      border-left:3px solid black;
    }
  `]
})
export class PlaylistsListComponent implements OnInit {

  // when playlist clicked...
  selectPlaylist(playlist){
    // publish playlist as $event to parent component:
    this.selectedChange.emit(playlist)
  }

  // Event name for angular - <component (select)="..."
  @Output('selectedChange')
  selectedChange = new EventEmitter()

  @Input()
  selected:Playlist;

  @Input()
  playlists: Playlist[] = []



  constructor() {

  }

  ngOnInit() {

  }

}