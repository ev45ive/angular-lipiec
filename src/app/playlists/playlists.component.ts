import { Component, OnInit, Inject } from '@angular/core';
import { Playlist } from './interfaces'
import { ActivatedRoute, Router } from '@angular/router'

import { PlaylistsService } from './playlists.service'

// <sg-playlists></sg-playlists>
@Component({
  selector: 'sg-playlists',
  template: `
    <div class="row">
      <div class="col">
        <sg-playlists-list [selected]="selectedPlaylist" (selectedChange)="select($event)" [playlists]="playlists"></sg-playlists-list>
      </div>
      <div class="col">

        <router-outlet></router-outlet> 

      </div>
    </div>   
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  selectedPlaylist;

  select(playlist){
    this.router.navigate(['playlists',playlist.id])
  }

  playlists:Playlist[] = []

  constructor(private service:PlaylistsService, private route:ActivatedRoute, private router:Router) { 
    
    service.getPlaylists().subscribe( playlists => this.playlists = playlists ) 

    this.route.firstChild.params.pluck('id')
    .switchMap( id => this.service.getPlaylist(id))
    .subscribe( playlist => {
      this.selectedPlaylist = playlist
    })
  }


  ngOnInit() {
  }

}
