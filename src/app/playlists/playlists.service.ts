import { Injectable } from '@angular/core';
import { Playlist } from './interfaces'
import { Http } from '@angular/http'

import { BehaviorSubject } from 'rxjs'

@Injectable()
export class PlaylistsService {
  
  constructor(private http:Http) {
    this.fetchPlaylists()
   }

  fetchPlaylists(){
     this.http.get(this.url)
              .map( response => response.json() )
              .subscribe( playlists => {
                this.playlists$.next(playlists)
              })
   }

  private playlists:Playlist[] = [
    { id: 1, name: 'The best of Angular', color: '#ff0000', favourite: false },
    { id: 2, name: 'Front-End Collection', color: '#00ff00', favourite: true },
    { id: 3, name: 'Angular hits, vol.2', color: '#0000ff', favourite: false }
  ]

  url = 'http://localhost:3000/playlists/'

  playlists$ = new BehaviorSubject<Playlist[]>(this.playlists)

  getPlaylists(){
    return this.playlists$
  }

  getPlaylist(id){
    return this.http.get(this.url + id)
              .map( response => response.json() )
  }

  savePlaylist(playlist){

    if(playlist.id){
      this.http.put(this.url + playlist.id, playlist).subscribe( response =>{
        this.fetchPlaylists()
      })

    }else{
      this.http.post(this.url, playlist).subscribe( response =>{
        this.fetchPlaylists()
      })
    }
  }



}
