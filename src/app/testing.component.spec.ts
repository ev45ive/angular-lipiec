import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser'

import { TestingComponent } from './testing.component';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        TestingComponent
      ],
      imports:[

      ],
      providers:[

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should render text', () => {
    component.message = "Testing message"
    fixture.detectChanges()
    var text = fixture.debugElement.query(By.css('p')).nativeElement.innerText
    expect(text).toEqual(component.message);
  });

  it('should render message form input', () => {
    fixture.debugElement.query(By.css('input')).triggerEventHandler('change',{target:{value:'Test'}})
    fixture.detectChanges()

    var text = fixture.debugElement.query(By.css('p')).nativeElement.innerText
    expect(text).toEqual('Test');
  });



});
