import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sg-testing',
  template: `
    <p>{{message}}</p>
    <input (change)="message = $event.target.value">
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = 'hello'

  constructor() { }

  ngOnInit() {
  }

}
